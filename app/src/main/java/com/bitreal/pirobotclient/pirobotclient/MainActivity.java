package com.bitreal.pirobotclient.pirobotclient;

import android.graphics.PixelFormat;
import android.media.MediaDataSource;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.rtp.RtpStream;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button)findViewById(R.id.button);
        btn.setOnClickListener(this);
    }

    /*
    private void playVideo1(String URL) {
        try {
            final VideoView videoView = (VideoView)findViewById(R.id.drive_video);
            Uri video = Uri.parse(URL);
            videoView.setVideoURI(video);
            videoView.start();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    */

    /*
    private void playVideo2(String URL) {
        MjpegView vv = (MjpegView)findViewById(R.id.drive_video);
        vv.setSource(URL);
    }
    */

    private void playVideo3(String url) {
        WebView wv = (WebView)findViewById(R.id.webView);
        wv.loadUrl(url);
    }

    @Override
    public void onClick(View v) {
        TextView textView = (TextView)findViewById(R.id.editText);
        //playVideo1(textView.getText().toString());
        //playVideo2(textView.getText().toString());
        playVideo3(textView.getText().toString());
    }
}
