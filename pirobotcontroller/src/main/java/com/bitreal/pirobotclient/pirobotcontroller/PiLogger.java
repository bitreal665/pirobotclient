package com.bitreal.pirobotclient.pirobotcontroller;

import android.content.Context;
import android.os.Handler;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by bitreal665 on 07.03.2017.
 */
public class PiLogger {

    public interface IPiLoggerListener {
        void onLogsChanged();
    }

    public static final int DEBUG  = 0;
    public static final int INFO  = 1;
    public static final int WARNING  = 2;
    public static final int ERROR  = 3;

    public class Log {
        private final DateFormat df;

        public Date date;
        public int level;
        public String tag;
        public String msg;

        public Log(int level, String tag, String msg) {
            this.df = new SimpleDateFormat("hh:mm:ss");
            this.date = new Date();
            this.level = level;
            this.tag = tag;
            this.msg = msg;
        }

        public String toLogString() {
            return String.format("[%1$s][%2$s][%3$s] %4$s\n", df.format(date), level, tag, msg);
        }
    }

    private final Context context;
    private ArrayList<Log> logs;
    private Object logsLock;
    private ArrayList<IPiLoggerListener> listeners;
    private Handler onLogChangedHandler;
    private long mainTreadId;

    public PiLogger(Context context) {
        this.context = context;
        logs = new ArrayList<>();
        logsLock = new Object();
        listeners = new ArrayList<>();
        onLogChangedHandler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                onLogChanged();
            }
        };
        mainTreadId = Thread.currentThread().getId();
    }

    public void log(int level, String tag, String msg) {
        // log
        switch (level) {
            case DEBUG:
                android.util.Log.d(tag, msg);
                break;
            case INFO:
                android.util.Log.i(tag, msg);
                break;
            case WARNING:
                android.util.Log.w(tag, msg);
                break;
            case ERROR:
                android.util.Log.e(tag, msg);
                break;
        }

        // log
        Log log = new Log(level, tag, msg);

        // add log
        synchronized (logsLock) {
            logs.add(log);
        }

        // listener
        if (Thread.currentThread().getId() == mainTreadId) {
            onLogChanged();
        } else {
            onLogChangedHandler.sendEmptyMessage(0);
        }
    }

    public void log(int level, String tag, Exception e) {
        e.printStackTrace();    // TODO

        log(level, tag, e.toString());
    }

    public ArrayList<Log> getLogs(int n) {
        return getLogs(n, DEBUG);
    }

    public ArrayList<Log> getLogs(int n, int level) {
        ArrayList<Log> aLogs = new ArrayList<>();
        synchronized (logsLock) {
            int i = logs.size() - 1;
            while (n > 0 && i >= 0) {
                if (logs.get(i).level >= level) {
                    aLogs.add(logs.get(i));
                    n--;
                }
                i--;
            }
        }
        return aLogs;
    }

    public int countLogs(int level) {
        int n = 0;
        synchronized (logsLock) {
            for (Log l : logs)
                if (l.level >= level)
                    n++;
        }
        return n;
    }

    public void addListener(IPiLoggerListener l) {
        listeners.add(l);
    }

    public void removeListener(IPiLoggerListener l) {
        listeners.remove(l);
    }

    private void onLogChanged() {
        for (IPiLoggerListener l : listeners) {
            l.onLogsChanged();
        }
    }
}
