package com.bitreal.pirobotclient.pirobotcontroller.PiCommands;

/**
 * Created by bitreal665 on 31.10.2017.
 */
public class EnableCameraCommand extends Command {

    private final boolean isEnable;

    public EnableCameraCommand(boolean isEnable) {
        super();
        this.isEnable = isEnable;
    }

    @Override
    public String toString() {
        return String.format("ENABLE_CAMERA %d", isEnable ? 1 : 0);
    }
}
