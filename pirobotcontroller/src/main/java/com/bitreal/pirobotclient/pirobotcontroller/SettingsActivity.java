package com.bitreal.pirobotclient.pirobotcontroller;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by bitreal665 on 06.03.2017.
 */
public class SettingsActivity extends Activity implements
        View.OnClickListener {

    private static final String TAG = "SettingsActivity";

    private PiLogger logger;
    private PiSettings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        logger = ((MyApp)getApplication()).getLogger();

        settings = ((MyApp)getApplication()).getSettings();

        setFloat(R.id.settings_left_k_field, settings.getLeftK());
        setFloat(R.id.settings_right_k_field, settings.getRightK());
        setFloat(R.id.settings_camera_k_field, settings.getCameraK());
        setBoolean(R.id.settings_invert_control_toggle, settings.isInvertControl());
        setBoolean(R.id.settings_invert_camera_toggle, settings.isInvertCamera());

        findViewById(R.id.settings_save_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_save_button:
                setFloat(R.id.settings_left_k_field, settings.setLeftK(parseFloat(R.id.settings_left_k_field, settings.getLeftK())));
                setFloat(R.id.settings_right_k_field, settings.setRighttK(parseFloat(R.id.settings_right_k_field, settings.getRightK())));
                setFloat(R.id.settings_camera_k_field, settings.setCameraK(parseFloat(R.id.settings_camera_k_field, settings.getCameraK())));
                setBoolean(R.id.settings_invert_control_toggle, settings.setInvertControl(parseBoolean(R.id.settings_invert_control_toggle)));
                setBoolean(R.id.settings_invert_camera_toggle, settings.setInvertCamera(parseBoolean(R.id.settings_invert_camera_toggle)));
                settings.save();
                Toast.makeText(this, "save", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }

    private void setFloat(int id, float val) {
        ((TextView)findViewById(id)).setText(new Float(val).toString());
    }

    private void setBoolean(int id, boolean val) {
        ((CheckBox)findViewById(id)).setChecked(val);
    }

    private float parseFloat(int id, float defaultVal) {
        float val = 0f;
        String strVal = ((TextView)findViewById(id)).getText().toString();
        try {
            val = Float.parseFloat(strVal);
        }
        catch (Exception e) {
            val = defaultVal;
        }
        return val;
    }

    private boolean parseBoolean(int id) {
        return  ((CheckBox)findViewById(id)).isChecked();
    }
}
