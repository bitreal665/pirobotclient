package com.bitreal.pirobotclient.pirobotcontroller.PiCommands;

/**
 * Created by bitreal665 on 07.03.2017.
 */
public class Command {

    private static int n = 0;

    protected final int id;

    protected Command() {
        id = n++;
    }

    public int getId() {
        return id;
    }
}
