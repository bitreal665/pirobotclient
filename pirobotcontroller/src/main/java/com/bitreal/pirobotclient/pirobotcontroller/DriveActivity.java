package com.bitreal.pirobotclient.pirobotcontroller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bitreal.pirobotclient.pirobotcontroller.PiCommands.DriveCommand;
import com.bitreal.pirobotclient.pirobotcontroller.PiCommands.EnableCameraCommand;
import com.bitreal.pirobotclient.pirobotcontroller.PiCommands.ExitCommand;
import com.bitreal.pirobotclient.pirobotcontroller.PiCommands.RotateCameraCommand;

/**
 * Created by bitreal665 on 06.03.2017.
 */
public class DriveActivity extends Activity implements
        View.OnTouchListener,
        View.OnClickListener,
        SeekBar.OnSeekBarChangeListener,
        PiClient.IListener,
        PiLogger.IPiLoggerListener {

    private static final String TAG = "DriveActivity";

    private static final int SETTINGS_MENU_ITEM = 0;
    private static final int LOGS_MENU_ITEM = 1;

    private PiLogger logger;
    private PiSettings settings;
    private PiClient client;

    private float left, right;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drive_layout);

        logger = ((MyApp)getApplication()).getLogger();

        settings = ((MyApp)getApplication()).getSettings();

        client = ((MyApp)getApplication()).getClient();
        client.addListener(this);

        left = right = 0f;

        findViewById(R.id.drive_left_forward_button).setOnTouchListener(this);
        findViewById(R.id.drive_left_back_button).setOnTouchListener(this);
        findViewById(R.id.drive_right_forward_button).setOnTouchListener(this);
        findViewById(R.id.drive_right_back_button).setOnTouchListener(this);

        findViewById(R.id.drive_reset_camera_button).setOnClickListener(this);
        findViewById(R.id.drive_disconnect_button).setOnClickListener(this);
        findViewById(R.id.drive_exit_button).setOnClickListener(this);
        findViewById(R.id.drive_logs_button).setOnClickListener(this);

        findViewById(R.id.drive_enable_camera_button).setOnClickListener(this);
        findViewById(R.id.drive_disable_camera_button).setOnClickListener(this);
        findViewById(R.id.drive_refresh_video_view_button).setOnClickListener(this);
        findViewById(R.id.drive_zoom_in_button).setOnClickListener(this);
        findViewById(R.id.drive_zoom_out_button).setOnClickListener(this);

        SeekBar seekBar = (SeekBar)findViewById(R.id.drive_rotate_camera_seek_bar);
        seekBar.setProgress(50);
        seekBar.setOnSeekBarChangeListener(this);

        onLogsChanged();
    }

    @Override
    protected void onDestroy() {
        client.removeListener(this);

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onFailConnect(PiClient.ConnectionErrors error) {

    }

    @Override
    public void onSuccessConnect() {

    }

    @Override
    public void onDisconnect(PiClient.DisconnectReasons reason) {
        Toast.makeText(this, String.format("disconnect (reason: %1$s)", reason), Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drive_reset_camera_button:
                ((SeekBar)findViewById(R.id.drive_rotate_camera_seek_bar)).setProgress(50);
                client.addCommand(new RotateCameraCommand(0f));
                break;

            case R.id.drive_disconnect_button:
                findViewById(R.id.drive_disconnect_button).setClickable(false);
                findViewById(R.id.drive_exit_button).setClickable(false);
                client.disconnect();
                Toast.makeText(this, "disconnect...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.drive_exit_button:
                findViewById(R.id.drive_disconnect_button).setClickable(false);
                findViewById(R.id.drive_exit_button).setClickable(false);
                client.addCommandAndDisconnect(new ExitCommand());
                Toast.makeText(this, "exit...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.drive_logs_button:
                startActivity(new Intent(this, LogsActivity.class));
                break;

            case R.id.drive_enable_camera_button:
                client.addCommand(new EnableCameraCommand(true));
                break;

            case R.id.drive_disable_camera_button:
                client.addCommand(new EnableCameraCommand(false));
                break;

            case R.id.drive_refresh_video_view_button:
                String url = "http://" + client.getLastUrl() + ":8000/stream.mjpg";
                ((WebView)findViewById(R.id.drive_video)).loadUrl(url);
                break;

            case R.id.drive_zoom_in_button:
                ((WebView)findViewById(R.id.drive_video)).zoomIn();
                break;

            case R.id.drive_zoom_out_button:
                ((WebView)findViewById(R.id.drive_video)).zoomOut();
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.drive_rotate_camera_seek_bar:
                float val = (seekBar.getProgress() * 0.02f - 1f) * settings.getCameraK();
                if (settings.isInvertCamera())
                    val *= -1f;
                client.addCommand(new RotateCameraCommand(val));
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.drive_left_forward_button:
            case R.id.drive_left_back_button:
            case R.id.drive_right_forward_button:
            case R.id.drive_right_back_button:
                processDriveButtons(v, event);
                break;

            default:
                throw new UnsupportedOperationException();
        }

        return false;
    }

    private void processDriveButtons(View v, MotionEvent event) {

        if (event.getAction() != MotionEvent.ACTION_DOWN
                && event.getAction() != MotionEvent.ACTION_UP
                && event.getAction() != MotionEvent.ACTION_CANCEL)
            return;

        float k = event.getAction() == MotionEvent.ACTION_DOWN ? 1f : 0f;

        switch (v.getId()) {
            case R.id.drive_left_forward_button:
                left = 1f * k;
                break;
            case R.id.drive_left_back_button:
                left = -1f * k;
                break;
            case R.id.drive_right_forward_button:
                right = 1f * k;
                break;
            case R.id.drive_right_back_button:
                right = -1f * k;
                break;

            default:
                throw new UnsupportedOperationException();
        }

        float _left = left;
        float _right = right;
        if (settings.isInvertControl()) {
            _left = right;
            _right = left;
        }
        _left *= settings.getLeftK();
        _right *= settings.getRightK();
        client.addCommand(new DriveCommand(_left, _right));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, SETTINGS_MENU_ITEM, 0, "settings");
        menu.add(0, LOGS_MENU_ITEM, 1, "logs");

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case SETTINGS_MENU_ITEM:
                startActivity(new Intent(this, SettingsActivity.class));
                break;

            case LOGS_MENU_ITEM:
                startActivity(new Intent(this, LogsActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLogsChanged() {
        ((Button)findViewById(R.id.drive_logs_button)).setText(String.format("LOGS(%d)", logger.countLogs(PiLogger.WARNING)));
    }
}
