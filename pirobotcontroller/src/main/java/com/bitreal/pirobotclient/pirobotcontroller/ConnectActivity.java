package com.bitreal.pirobotclient.pirobotcontroller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by bitreal665 on 06.03.2017.
 */
public class ConnectActivity extends Activity implements
        View.OnClickListener,
        PiClient.IListener {

    private static final String TAG = "ConnectActivity";

    private static final int SETTINGS_MENU_ITEM = 0;
    private static final int LOGS_MENU_ITEM = 1;

    private static final String URL1 = "192.168.0.102";
    private static final String URL2 = "192.168.0.103";
    private static final String URL3 = "109.167.141.224";

    private PiLogger logger;
    private PiSettings settings;
    private PiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connect_layout);

        logger = ((MyApp)getApplication()).getLogger();

        settings = ((MyApp)getApplication()).getSettings();

        client = ((MyApp)getApplication()).getClient();
        client.addListener(this);

        findViewById(R.id.connect_ip1_button).setOnClickListener(this);
        findViewById(R.id.connect_ip2_button).setOnClickListener(this);
        findViewById(R.id.connect_ip3_button).setOnClickListener(this);
        findViewById(R.id.connect_button).setOnClickListener(this);
        findViewById(R.id.connection_log_button).setOnClickListener(this);
        findViewById(R.id.connection_settings_button).setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        client.removeListener(this);

        super.onDestroy();
    }

    @Override
    public void onFailConnect(PiClient.ConnectionErrors error) {
        findViewById(R.id.connect_button).setClickable(true);
        Toast.makeText(this, String.format("fail connect (error: %1$s)", error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccessConnect() {
        findViewById(R.id.connect_button).setClickable(true);
        Toast.makeText(this, "success connect", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, DriveActivity.class));
    }

    @Override
    public void onDisconnect(PiClient.DisconnectReasons reason) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.connect_ip1_button:
                ((TextView)findViewById(R.id.connect_url_field)).setText(URL1);
                break;

            case R.id.connect_ip2_button:
                ((TextView)findViewById(R.id.connect_url_field)).setText(URL2);
                break;

            case R.id.connect_ip3_button:
                ((TextView)findViewById(R.id.connect_url_field)).setText(URL3);
                break;

            case R.id.connect_button:
                findViewById(R.id.connect_button).setClickable(false);

                String url = ((TextView)findViewById(R.id.connect_url_field)).getText().toString();
                String port = ((TextView)findViewById(R.id.connect_port_field)).getText().toString();
                String pass = ((TextView)findViewById(R.id.connect_pass_field)).getText().toString();
                client.connect(url, port);
                Toast.makeText(this, "connect...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.connection_log_button:
                startActivity(new Intent(this, LogsActivity.class));
                break;

            case R.id.connection_settings_button:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, SETTINGS_MENU_ITEM, 0, "settings");
        menu.add(0, LOGS_MENU_ITEM, 1, "logs");

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case SETTINGS_MENU_ITEM:
                startActivity(new Intent(this, SettingsActivity.class));
                break;

            case LOGS_MENU_ITEM:
                startActivity(new Intent(this, LogsActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
