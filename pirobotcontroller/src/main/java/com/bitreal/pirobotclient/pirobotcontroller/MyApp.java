package com.bitreal.pirobotclient.pirobotcontroller;

import android.app.Application;

/**
 * Created by bitreal665 on 06.03.2017.
 */
public class MyApp extends Application {

    private static final String SETTINGS_NAME = "settings";

    private PiLogger logger;
    private PiSettings settings;
    private PiClient client;

    @Override
    public void onCreate ()
    {
        logger = new PiLogger(this);
        settings = new PiSettings(getSharedPreferences(SETTINGS_NAME, MODE_PRIVATE), logger);
        client = new PiClient(logger);

        super.onCreate();
    }

    public PiClient getClient()
    {
        return client;
    }

    public PiSettings getSettings() { return settings; }

    public PiLogger getLogger() { return logger; }
}
