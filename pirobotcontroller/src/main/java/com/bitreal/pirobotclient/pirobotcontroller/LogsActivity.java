package com.bitreal.pirobotclient.pirobotcontroller;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by bitreal665 on 06.03.2017.
 */
public class LogsActivity extends Activity implements PiLogger.IPiLoggerListener {

    private static final String TAG = "LogsActivity";

    private PiLogger logger;
    private PiSettings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logs_layout);

        logger = ((MyApp)getApplication()).getLogger();

        settings = ((MyApp)getApplication()).getSettings();

        onLogsChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        logger.addListener(this);
    }

    @Override
    protected void onPause() {
        logger.removeListener(this);
        super.onPause();
    }

    @Override
    public void onLogsChanged() {
        StringBuilder sb = new StringBuilder();
        for (PiLogger.Log l : logger.getLogs(500, PiLogger.INFO)) {
            sb.append(l.toLogString());
        }
        ((TextView)findViewById(R.id.logs_logs_text)).setText(sb.toString());
    }
}
