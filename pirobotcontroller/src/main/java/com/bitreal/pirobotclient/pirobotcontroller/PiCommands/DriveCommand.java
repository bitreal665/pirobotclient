package com.bitreal.pirobotclient.pirobotcontroller.PiCommands;

/**
 * Created by bitreal665 on 07.03.2017.
 */
public class DriveCommand extends Command {

    private final float left;
    private final float right;

    public DriveCommand(float l, float r) {
        super();
        left = l;
        right = r;
    }

    @Override
    public String toString() {
        //return String.format("DRIVE(%1$.3f, %2$.3f);\n", left, right);
        return String.format("DRIVE %s %s", String.valueOf(left), String.valueOf(right));
    }
}
