package com.bitreal.pirobotclient.pirobotcontroller;

import android.os.Handler;
import android.os.Message;

import com.bitreal.pirobotclient.pirobotcontroller.PiCommands.Command;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by bitreal665 on 07.03.2017.
 */
public class PiClient implements Runnable {

    enum ConnectionErrors {
        AlreadyConnected,
        BadPort,

        SocketError,
        IOError,
    }

    enum DisconnectReasons {
        Eof,

        Error,
    }

    public interface IListener {
        void onFailConnect(ConnectionErrors error);
        void onSuccessConnect();
        void onDisconnect(DisconnectReasons reason);
    }

    private static final String TAG = "PiClient";

    private final PiLogger logger;

    private ArrayList<IListener> listeners;

    private String url;
    private int port;
    private Thread thread;
    private final Object lock;
    private ArrayList<Command> commands;
    private boolean loopActive;
    private int lastCommandId;

    private Handler onFailConnectHandler;
    private Handler onSuccessConnectHandler;
    private Handler onDisconnectHandler;

    public PiClient(PiLogger logger) {
        this.logger = logger;
        this.listeners = new ArrayList<>();
        this.commands = new ArrayList<>();
        this.lock = new Object();
    }

    public void connect(String url, String port) {

        logger.log(PiLogger.INFO, TAG, String.format("connection request (url:%1$s port:%2$s)", url, port));

        if (isLoopActive()) {
            logger.log(PiLogger.WARNING, TAG, "loop already active");
            onFailConnect(ConnectionErrors.AlreadyConnected);
            return;
        }

        this.url = url;
        try {
            this.port = Integer.parseInt(port);
        }
        catch (Exception e) {
            logger.log(PiLogger.WARNING, TAG, e);
            onFailConnect(ConnectionErrors.BadPort);
            return;
        }

        onFailConnectHandler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                onFailConnect((ConnectionErrors)msg.obj);
            }
        };
        onSuccessConnectHandler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                onSuccessConnect();
            }
        };
        onDisconnectHandler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                onDisconnect((DisconnectReasons)msg.obj);
            }
        };

        loopActive = true;
        lastCommandId = -1;
        thread = new Thread(this);
        thread.setName("client_thread");
        thread.start();
    }

    public void disconnect() {
        logger.log(PiLogger.INFO, TAG, String.format("disconnect request"));

        if (!isLoopActive())
        {
            logger.log(PiLogger.WARNING, TAG, String.format("loop not active"));
            return;
        }

        synchronized (lock) {
            loopActive = false;
        }

        synchronized (thread) {
            if (thread.getState() == Thread.State.WAITING)
                thread.notify();
        }
    }

    public void addCommand(Command cmd) {
        logger.log(PiLogger.INFO, TAG, String.format("cmd request (cmd:%1$s)", cmd.toString()));

        if (!isLoopActive())
        {
            logger.log(PiLogger.WARNING, TAG, String.format("loop not active"));
            return;
        }

        synchronized (lock) {
            commands.add(cmd);
        }

        synchronized (thread) {
            if (thread.getState() == Thread.State.WAITING)
                thread.notify();
        }
    }

    public void addCommandAndDisconnect(Command cmd) {
        synchronized (lock) {
            lastCommandId = cmd.getId();
        }
        addCommand(cmd);
    }

    public void addListener(IListener l) {
        synchronized (lock) {
            listeners.add(l);
        }
    }

    public void removeListener(IListener l) {
        synchronized (lock) {
            listeners.remove(l);
        }
    }

    public String getLastUrl() {
        return url;
    }

    private void onSuccessConnect() {
        logger.log(PiLogger.INFO, TAG, String.format("onSuccessConnect"));

        ArrayList<IListener> _listeners = null;
        synchronized (lock) {
            _listeners = new ArrayList<>(listeners);
        }

        for(IListener l : _listeners)
            l.onSuccessConnect();
    }

    private void onFailConnect(ConnectionErrors error) {
        logger.log(PiLogger.WARNING, TAG, String.format("onFailConnect (error:%1$s)", error));

        ArrayList<IListener> _listeners = null;
        synchronized (lock) {
            _listeners = new ArrayList<>(listeners);
        }

        for(IListener l : _listeners)
            l.onFailConnect(error);
    }

    private void onDisconnect(DisconnectReasons reason) {
        int logLevel = reason == DisconnectReasons.Eof ? PiLogger.INFO : PiLogger.WARNING;
        logger.log(logLevel, TAG, String.format("onDisconnect (reason:%1$s)", reason));

        ArrayList<IListener> _listeners;
        synchronized (lock) {
            _listeners = new ArrayList<>(listeners);
        }

        for(IListener l : _listeners)
            l.onDisconnect(reason);
    }

    @Override
    public void run() {
        boolean isConnectionError = false;
        ConnectionErrors connectError = ConnectionErrors.AlreadyConnected;
        DisconnectReasons disconnectReason = DisconnectReasons.Eof;

        Socket socket = null;
        DataOutputStream out = null;
        DataInputStream in = null;

        // socket
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(url, port), 5000);
            logger.log(PiLogger.INFO, TAG, String.format("socket open"));
        }
        catch (Exception e) {
            logger.log(PiLogger.WARNING, TAG, e);
            isConnectionError = true;
            connectError = ConnectionErrors.SocketError;
        }

        // out
        if (!isConnectionError) {
            try {
                out = new DataOutputStream(socket.getOutputStream());
                logger.log(PiLogger.INFO, TAG, String.format("out open"));
            }
            catch (Exception e) {
                logger.log(PiLogger.WARNING, TAG, e);
                isConnectionError = true;
                connectError = ConnectionErrors.IOError;
            }
        }

        // in
        if (!isConnectionError) {
            try {
                in = new DataInputStream(socket.getInputStream());
                logger.log(PiLogger.INFO, TAG, String.format("in open"));
            }
            catch (Exception e) {
                logger.log(PiLogger.WARNING, TAG, e);
                isConnectionError = true;
                connectError = ConnectionErrors.IOError;
            }

            onSuccessConnectHandler.sendEmptyMessage(0);
        }

        // loop
        if (!isConnectionError)
        {
            disconnectReason = mainLoop(out, in);
        }

        try {
            if (in != null)
            {
                in.close();
                logger.log(PiLogger.INFO, TAG, String.format("in close"));
            }

            if (out != null)
            {
                out.close();
                logger.log(PiLogger.INFO, TAG, String.format("out close"));
            }

            if (socket != null)
            {
                socket.close();
                logger.log(PiLogger.INFO, TAG, String.format("socket close"));
            }
        }
        catch (Exception e) {
            logger.log(PiLogger.WARNING, TAG, e);
        }

        synchronized (lock) {
            loopActive = false;
        }

        if (isConnectionError)
        {
            Message msg = new Message();
            msg.obj = connectError;
            onFailConnectHandler.sendMessage(msg);
        }

        if (!isConnectionError) {
            Message msg = new Message();
            msg.obj = disconnectReason;
            onDisconnectHandler.sendMessage(msg);
        }
    }

    private DisconnectReasons mainLoop(DataOutputStream out, DataInputStream in) {
        byte[] respBytes = new byte[1024];

        try {

            while (isLoopActive()) {

                while (isExistCommands() && isLoopActive()) {
                    Command cmd = popNextCommand();

                    // write
                    String reqStr = cmd.toString();
                    byte[] reqBytes = reqStr.getBytes(Charset.forName("UTF-8"));
                    logger.log(PiLogger.DEBUG, TAG, String.format("write '%s' (%d)", reqStr, reqBytes.length));
                    long t1 = System.currentTimeMillis();
                    out.write(reqBytes);
                    out.flush();

                    // read
                    long t2 = System.currentTimeMillis();
                    int respLength = in.read(respBytes);
                    long t3 = System.currentTimeMillis();
                    if (respLength > 0) {
                        if (respLength >= 1024) {
                            logger.log(PiLogger.WARNING, TAG, String.format("long response"));
                        }
                        if (t2 - t1 > 100) {
                            logger.log(PiLogger.WARNING, TAG, String.format("big request time"));
                        }
                        if (t3 - t2 > 100) {
                            logger.log(PiLogger.WARNING, TAG, String.format("big response time"));
                        }
                        String respStr = new String(respBytes, 0, respLength, Charset.forName("UTF-8"));
                        logger.log(PiLogger.DEBUG, TAG, String.format("read '%s' (%d)", respStr, respLength));
                    }
                    else {
                        logger.log(PiLogger.WARNING, TAG, String.format("empty response"));
                        return DisconnectReasons.Error;
                    }

                    //
                    int _lastCmdId = getLastCommandId();
                    if (_lastCmdId >= 0 && _lastCmdId <= cmd.getId()) {
                        logger.log(PiLogger.INFO, TAG, String.format("was last cmd"));
                        return DisconnectReasons.Eof;
                    }
                }

                // ждем следующей команды
                if (isLoopActive())
                {
                    synchronized (thread) {
                        thread.wait();
                    }
                }
            }

        }
        catch (Exception e) {
            logger.log(PiLogger.WARNING, TAG, e);
            return DisconnectReasons.Error;
        }

        return DisconnectReasons.Eof;
    }

    private boolean isLoopActive() {
        synchronized (lock) {
            return loopActive;
        }
    }

    private int getLastCommandId() {
        synchronized (lock) {
            return lastCommandId;
        }
    }

    private boolean isExistCommands() {
        synchronized (lock) {
            return !commands.isEmpty();
        }
    }

    private Command popNextCommand() {
        synchronized (lock) {
            return commands.remove(0);
        }
    }
}
