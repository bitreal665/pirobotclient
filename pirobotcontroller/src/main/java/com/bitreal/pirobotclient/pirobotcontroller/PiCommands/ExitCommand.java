package com.bitreal.pirobotclient.pirobotcontroller.PiCommands;

/**
 * Created by bitreal665 on 13.03.2017.
 */
public class ExitCommand extends Command {

    public ExitCommand() {
        super();
    }

    @Override
    public String toString() {
        return String.format("EXIT");
    }
}
