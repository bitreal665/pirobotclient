package com.bitreal.pirobotclient.pirobotcontroller.PiCommands;

/**
 * Created by bitreal665 on 07.03.2017.
 */
public class RotateCameraCommand extends Command {

    private final float angle;

    public RotateCameraCommand(float a) {
        super();
        angle = a;
    }

    @Override
    public String toString() {
        //return String.format("ROTATE_CAMERA(%1$.3f);\n", angle);
        return String.format("ROTATE_CAMERA %s", String.valueOf(angle));
    }
}
