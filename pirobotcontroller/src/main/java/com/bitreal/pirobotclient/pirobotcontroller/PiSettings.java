package com.bitreal.pirobotclient.pirobotcontroller;

import android.content.SharedPreferences;

/**
 * Created by bitreal665 on 08.03.2017.
 */
public class PiSettings {

    private static final String TAG = "PiSettings";

    private static final String LEFT_K_NAME  = "left_k";
    private static final String RIGHT_K_NAME  = "right_k";
    private static final String CAMERA_K_NAME  = "camera_k";
    private static final String INVERT_CONTROL_NAME  = "invert_control";
    private static final String INVERT_CAMERA_NAME  = "invert_camera";

    private final PiLogger logger;

    private float leftK;
    private float rightK;
    private float cameraK;
    private boolean invertControl;
    private boolean invertCamera;

    private final SharedPreferences sPref;

    public PiSettings(SharedPreferences sPref, PiLogger logger) {
        this.sPref = sPref;
        this.logger = logger;
        setLeftK(sPref.getFloat(LEFT_K_NAME, 1f));
        setRighttK(sPref.getFloat(RIGHT_K_NAME, 1f));
        setCameraK(sPref.getFloat(CAMERA_K_NAME, 1f));
        setInvertControl(sPref.getBoolean(INVERT_CONTROL_NAME, true));
        setInvertCamera(sPref.getBoolean(INVERT_CAMERA_NAME, true));
    }

    public float getLeftK() { return leftK; }   public float setLeftK(float val) { leftK = Math.min(Math.max(0f, val), 1f); return leftK; }
    public float getRightK() { return rightK; }   public float setRighttK(float val) { rightK = Math.min(Math.max(0f, val), 1f); return rightK; }
    public float getCameraK() { return cameraK; }   public float setCameraK(float val) { cameraK = Math.min(Math.max(0f, val), 1f); return cameraK; }
    public boolean isInvertControl() { return invertControl; } public boolean setInvertControl(boolean val) { invertControl = val; return invertControl; }
    public boolean isInvertCamera() { return invertCamera; } public boolean setInvertCamera(boolean val) { invertCamera = val; return invertCamera; }

    public void save() {
        SharedPreferences.Editor ed = sPref.edit();
        ed.putFloat(LEFT_K_NAME, leftK);
        ed.putFloat(RIGHT_K_NAME, rightK);
        ed.putFloat(CAMERA_K_NAME, cameraK);
        ed.putBoolean(INVERT_CONTROL_NAME, invertControl);
        ed.putBoolean(INVERT_CAMERA_NAME, invertCamera);
        ed.commit();
        logger.log(PiLogger.INFO, TAG, "save settings");
    }
}
